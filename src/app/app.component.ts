import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Investor } from './model/investor';
import { AppService } from './app.service';
import { invertColor } from '@swimlane/ngx-charts/release/utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  investorForm: FormGroup;
  investors: Investor[] = [];
  data = [];

  constructor(private formBuilder: FormBuilder, private appService: AppService) { }

  ngOnInit() {
    this.investorForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      participation: ['', Validators.required],
    });

    this.appService.findAllInvestors().subscribe((investors: any[]) => {
      investors.forEach(investor => {
        this.investors.push(investor as Investor);
      });
      this.updateData();
    });
  }

  create() {
    const investor = this.investorForm.value as Investor;

    this.appService.createInvestor(investor).subscribe((res) => {
      this.investors.push(res.body as Investor);
      this.updateData();
      this.investorForm.reset();
    }, (error) => {
      console.log(error);
    })
  }

  private updateData() {
    this.data = this.investors.map((investor) => {
      return {
        name: `${investor.firstName} ${investor.lastName}`,
        value: investor.participation
      }
    });
  }
}
