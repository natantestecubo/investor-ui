export class Investor {
  firstName: string;
  lastName: string;
  participation: number;
}
