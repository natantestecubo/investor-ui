import { Injectable } from "@angular/core";
import {HttpClient} from '@angular/common/http';
import { Investor } from "./model/investor";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private httpClient: HttpClient) {}

  createInvestor(investor: Investor) {
    return this.httpClient.post(environment.api, investor, { observe: 'response' });
  }

  findAllInvestors() {
    return this.httpClient.get(environment.api);
  }
}
